/* global exports */
"use strict";

// module Main

exports.setInterval = function(f) {
    return function(time) {
        return function() {
            return window.setInterval(f, time);
        }
    }
};

exports.clearInterval = function(n) {
    return function() {
        return window.clearInterval(n);
    }
};
