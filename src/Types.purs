module Types where
import Data.Maybe (Maybe(..))
import Prelude (class Show, (++), bind, (<$>), show, ($), return)
import Data.Foreign.Class (class IsForeign, readProp, readJSON, read) 
import Data.Foreign.Null (runNull)
import Data.Foreign (readString) 
import Data.Either (Either(Right, Left)) 
import React (ReactElement)
import React.DOM (text)
import Data.String (take)

newtype User = User
  { login :: String
  , email :: Maybe String
  , username  :: String
  } 


instance showUser :: Show User where
    show (User u)  = case u.email of
                  Nothing -> "User: " ++ u.login ++ " " ++ u.username
                  Just e -> "User: " ++ u.login ++ " " ++ e ++ " " ++ u.username

instance userIsForeign :: IsForeign User where
    read value = do
        login <- readProp "login" value
        user <- readProp "user" value
        email <- runNull <$> readProp "email" value
        return $ User { login: login, email: email, username: user }

instance usersIsForeign :: IsForeign Users where
    read value = Users <$> read value 

instance dockersIsForeign :: IsForeign Dockers where
    read value = Dockers <$> read value

instance logIsForeign :: IsForeign Log where
    read value = Log <$> read value

data Status = Stopped
            | Removed
            | Running
            | Error String

instance showStatus :: Show Status where
    show Stopped = "stopped"
    show Running = "running"
    show Removed = "removed"
    show (Error s) = s

instance statusIsForeign :: IsForeign Status where
    read v = case readString v of
                  Right "Stopped" -> Right Stopped
                  Right "Running" -> Right Running
                  Right "Removed" -> Right Removed
                  Right s -> Right (Error s)
                  Left e -> Left e

type Commit = String
type DockerID = Maybe String
type IP = Maybe String

newtype Docker = Docker
  { docker :: DockerID
  , commit :: Commit
  , user :: User
  , status :: Status
  , ip :: IP
  , time :: String
  , message :: String
  }

instance showDocker :: Show Docker where
    show (Docker v) = show v.docker ++ " " ++ v.commit ++ " " ++ show v.user ++ " " ++ show v.status ++ " " ++ show v.ip ++ v.message

instance dockerIsForeign :: IsForeign Docker where
    read value = do
        login <- readProp "login" value
        user <- readProp "user" value
        email <- runNull <$> readProp "email" value
        docker <- runNull <$> readProp "docker" value
        commit <- readProp "commit" value
        status <- readProp "status" value
        ip <- runNull <$> readProp "ip" value
        time <- readProp "time" value
        message <- readProp "message" value
        return $ Docker
          { docker: docker
          , commit: commit
          , user: User
            { login: login
            , username: user
            , email: email
            }
          , ip: ip
          , status: status
          , time : time
          , message : message
          }

type Url = { url :: String }

class TXT a where
    txt :: a -> Array ReactElement

newtype Ims = Ims {length :: Int, string :: DockerID}

instance imsTXT :: TXT Ims where
    txt (Ims x) = case x.string of
                       Nothing -> []
                       Just t -> [ text (take x.length t) ]

instance sTXT :: TXT String where
    txt t = [ text t ]

instance msTXT :: TXT (Maybe String) where
    txt Nothing = []
    txt (Just t) = [ text t ]

newtype Users = Users (Array User)
newtype Dockers = Dockers (Array Docker)
newtype Log = Log String

class (IsForeign items) <= Loadable items where
    unpack :: String -> items

instance usersLoadable :: Loadable Users where
    unpack response = 
        case readJSON response of
             Left _ -> Users []
             Right o -> Users o

instance dockersLoadable :: Loadable Dockers where
    unpack response =
        case readJSON response of
             Left _ -> Dockers []
             Right o -> Dockers o

instance logLoadable :: Loadable Log where
    unpack response = Log response




