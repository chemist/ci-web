module Main where

import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Class (liftEff)
import Control.Monad.Eff.Console (CONSOLE, log, print)
import Data.Maybe.Unsafe (fromJust)
import Data.String (take)
import Data.Nullable (toMaybe)
import Prelude (unit, Unit, return, ($), bind, show, (<$>), (>>=), map, (++), void, (<<<)) 
import Control.Apply ((*>))

import DOM (DOM())
import DOM.HTML (window)
import DOM.HTML.Document (body)
import DOM.HTML.Types (htmlElementToElement)
import DOM.HTML.Window (document)
import DOM.Node.Types (Element())

import React (ReactElement, ReactClass, ReactState, ReactProps, ReadWrite, ReactThis, render, createFactory, createClass, readState, spec, getProps, transformState) 
import React.DOM as D
import React.DOM.Props as P
import React.Bootstrap as B
import Network.HTTP.Affjax (AJAX, get, put, post, delete) 
import Control.Monad.Aff (runAff) 
import Routie (route0, route1, ROUTIE)

import Types (Log(..), Dockers(..), Users(..), class Loadable, unpack, Url, Docker(..), User(..), Status(..), txt, Ims(..), Commit)

foreign import setInterval :: forall eff a. Eff eff a -> Int -> Eff eff Number

foreign import clearInterval :: forall eff. Number -> Eff eff Unit

usersUrl :: String 
usersUrl = "http://ci:8080/users"

dockersUrl :: String 
dockersUrl = "http://ci:8080/dockers"

logUrl :: String
logUrl = "http://ci:8080/log"

slogUrl :: String
slogUrl = "http://ci:8080/slog"

main :: forall eff. Eff ( console :: CONSOLE, r :: ROUTIE, dom :: DOM, ajax :: AJAX | eff ) Unit
main = do
    route0 "" (component dockersApp { url : dockersUrl })
    route0 "users" (component usersApp { url : usersUrl })
    route1 "commit log/:commit" (\commit -> component logApp { url : (logUrl ++ "/" ++ commit) })
    route1 "commit slog/:commit" (\commit -> component logApp { url : (slogUrl ++ "/" ++ commit) })
    return unit

component :: forall props eff. ReactClass props -> props -> Eff ( ajax :: AJAX, console :: CONSOLE, dom :: DOM | eff ) Unit
component app props = (container >>= render ui) *> return unit
  where
      ui :: ReactElement
      ui = D.div [ P.className "show-grid" ] 
                 [ D.div [ P.className "col-md-8" ]
                         [ createFactory app props ]
                 ]

container :: forall eff. Eff (ajax :: AJAX, console :: CONSOLE, dom :: DOM | eff) Element
container = do
  win <- window
  doc <- document win
  elm <- fromJust <$> toMaybe <$> body doc
  return $ htmlElementToElement elm


logApp :: ReactClass Url
logApp = createClass (spec { interval : 0.0, items : Log "" } render)
    { componentDidMount = \this -> do
        load this
        n <- setInterval (load this) 3000
        transformState this $ \st -> { interval : n, items : st.items }
        return unit
    , componentWillUnmount = \this -> do
        st <- readState this
        clearInterval st.interval
    }
      where
      log (Log x) = x
      render this = do
          st <- readState this
          return $ D.pre [ P.className "log" ]
                         (txt (log st.items))

dockersApp :: ReactClass Url
dockersApp = createClass (spec {interval : 0.0, items : Dockers []} render)
    { componentDidMount = \this -> do
        load this
        n <- setInterval (load this) 3000
        transformState this $ \st -> { interval : n, items : st.items }
        return unit
    , componentWillUnmount = \this -> do
        st <- readState this
        clearInterval st.interval
    }
  where
  dockers (Dockers u) = u
  render this = do
    st <- readState this
    return $ D.table 
      [ P.className "table" ]
      [ dockersHeader
      , D.tbody' $ map dockerElement (dockers st.items)
      ]
  dockersHeader = D.thead'
    [ D.tr'
      [ D.th' (txt "time")
      , D.th' (txt "user")
      , D.th' (txt "id")
      , D.th' (txt "commit")
      , D.th' (txt "message")
      , D.th' (txt "ip")
      , D.th' (txt "log")
      , D.th' (txt "slog")
      , D.th' (txt "managment")
      ]
    ]
  dockerElement (Docker d) = D.tr'
    [ D.td' (txt $ d.time)
    , D.td' (txt $ username d.user)
    , D.td' (txt (Ims { length : 5, string : d.docker})) 
    , D.td' (txt (take 5 d.commit))
    , D.td' (txt $ d.message)
    , D.td' (txt d.ip)
    , D.td' 
      [ D.a 
        [ P.href ("#log/" ++ d.commit)
        , P.onClick (\_ -> return unit)
        ] 
        (txt "log") 
      ]
    , D.td' 
      [ D.a 
        [ P.href ("#slog/" ++ d.commit)
        , P.onClick (\_ -> return unit)
        ] 
        (txt "slog") 
      ]
    , D.td'  (labelButton d.status d.commit)
    ]
  username (User x) = x.username

usersApp :: ReactClass Url
usersApp = createClass (spec {interval : 0.0, items : Users [] } render)
    { componentDidMount = \this -> do
        print "update users list"
        load this
        n <- setInterval (load this) 3000
        transformState this $ \st -> { interval : n, items : st.items }
        return unit
    , componentWillUnmount = \this -> do
        st <- readState this
        clearInterval st.interval
    }
  where
    render this = do
      st <- readState this
      let users (Users u) = u
      return $ D.table [ P.className "table"]
                       [ usersHeader
                       , D.tbody' $ map userElement (users st.items)
                       ]
    usersHeader = D.thead' 
      [ D.tr'  
        [ D.th' (txt "login")
        , D.th' (txt "user" )
        , D.th' (txt "email")
        ]
      ]
    userElement (User user) = D.tr'
      [ D.td' (txt user.login)
      , D.td' (txt user.username) 
      , D.td' (txt user.email) 
      ]

labelStatus :: Status -> ReactElement
labelStatus Running = D.span [ P.className "label label-success glyphicon glyphicon-play" ] [D.text ""]
labelStatus Stopped = D.span [ P.className "label label-info glyphicon glyphicon-stop" ] [D.text ""]
labelStatus Removed = D.span [ P.className "label label-default glyphicon glyphicon-trash" ] [D.text ""]
labelStatus _       = D.span [ P.className "label label-danger glyphicon glyphicon-fire" ] [D.text ""]

labelButton :: Status -> Commit -> Array ReactElement
labelButton (Error _) _ = [ B.dropdownButton { title: labelStatus (Error ""), bsSize: "xsmall" } []]
labelButton Running commit = [ B.dropdownButton { title: labelStatus Running, bsSize: "xsmall" }
                               [ startStop Stopped commit
                               , startStop Removed commit
                               ]
                             ]
labelButton Stopped commit = [ B.dropdownButton { title: labelStatus Stopped, bsSize: "xsmall" }
                               [ startStop Running commit
                               , startStop Removed commit
                               ]
                             ]
labelButton Removed commit = [ B.dropdownButton { title: labelStatus Removed
                                                , bsSize: "xsmall" 
                                                }
                               [ startStop Running commit ]
                             ]

startStop :: Status -> Commit -> ReactElement
startStop status commit = B.menuItem_ 
    [ D.div
      [ P.onClick (manageDocker status commit) ]
      [ labelStatus status ]
    ]

manageDocker :: forall eff event. Status -> Commit -> event -> Eff ( ajax :: AJAX, console :: CONSOLE | eff ) Unit
manageDocker status commit _ = do
    log $ "manage " ++ show (status :: Status) ++ (commit :: Commit)
    let url = "http://ci:8080/docker/" ++ commit
    case status of
         Running -> runAff err ok (put url "put")
         Stopped -> runAff err ok (post url "post")
         Removed -> runAff err ok (delete url)
         _ -> return unit
    where
      err = log <<< show 
      ok r = log r.response

load :: forall eff items. Loadable items => ReactThis Url {interval :: Number, items :: items } 
        -> Eff ( props :: ReactProps
               , state :: ReactState ReadWrite
               , console :: CONSOLE
               , ajax :: AJAX 
               | eff) Unit 
load this = do
    props <- getProps this
    runAff err ok (get props.url )
    where
      err = log <<< show 
      ok r = void $ liftEff $ transformState this $ \st -> { interval : st.interval, items : (unpack r.response) }

